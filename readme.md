
## PD Stevedore Unbottler

Extracts the second document of a yaml file when it lists this plugin in the first document. File extension is html, unless specified via key "text_format". PD indicates this is made for use with other PDistillery components.

* -o &lt;path&gt; path to output folder
* -f [path path ...] paths to files to transform to here, preferably not from here
* -v verbose mode
* -h show the list of arguments

Released under CDDL v1.0 terms. 

Depends on YamlBeans and Commons Cli.
